<?php

include 'utils.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {
    // Render index view
    return $this->view->render($response, 'index.latte');
})->setName('index');

$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

    // Users page endpoint
    $app->get('/users', function (Request $request, Response $response, $args) {
        // kod pro zobrazeni JSON filu na strance
        // $url = 'https://akela.mendelu.cz/~xvalovic/mock_users.json';
        // $users = file_get_contents($url);
        // $user_arr = json_decode($users, true);
        // $data['users'] = json_decode($users, true);

        // echo var_dump($user_arr);     - vytiskne cele pole json

        $params = $request->getQueryParams(); # vytvori se asociativni pole 'query' => 'bill'

        if (empty($params['query'])) {
            $stmt = $this->db->prepare('SELECT * FROM person ORDER BY first_name');
        } else {
            $stmt = $this->db->prepare('SELECT * FROM person 
            WHERE lower(first_name) = lower(:fn) OR last_name = :ln');
            $stmt->bindParam(':fn', $params['query']);
            $stmt->bindParam(':ln', $params['query']);
            //ochrana proti SQL injection
        }

        $stmt->execute();
        $data['users'] = $stmt->fetchall();

        return $this->view->render($response, 'users.latte', $data);
    })->setName('users_list');

    // Login page endpoint
    $app->get('/login', function (Request $request, Response $response, $args) {
        return $this->view->render($response, 'login.latte');
    })->setName('login_form');

    // zpracuje a overi uzivatele
    $app->post('/login', function (Request $request, Response $response, $args) {
        $formData = $request->getParsedBody();
        // $formData = ['username' => 'Johny', 'passwd' => 'heslo123']

        // vypocita hash hesla
        $passwd_hash = hash('sha256', $formData['passwd']);

        // overime uzivatele v DBS
        $stmt = $this->db->prepare('SELECT * FROM person WHERE 
                                    lower(nickname) = lower(:nn) AND
                                    password = :pswd'
                                    );
        $stmt->bindValue(':nn', $formData['nickname']);
        $stmt->bindValue(':pswd', $passwd_hash);
        $stmt->execute();
        $logged_user = $stmt->fetch(); // ['nickname' => 'aaa']
        // echo var_dump($logged_user);

        if ($logged_user) {
            // $_session uklada id v cookies napric vsechny http requesty
            // muzem pristupovat napric ruznyma endpointama
            $_SESSION['logged_user'] = $logged_user;
            return $response->withHeader('Location', $this->router->pathFor('index'));
        } else {
            return $this->view->render($response, 'login.latte', ['message' => 'Invalid creditentials']);
        }

        return $this->view->render($response, 'login.latte', $formData);
    });


$app->group('/auth', function() use($app) {

    // Logout endpoint
    $app->get('/logout', function (Request $request, Response $response, $args) {
        session_destroy();
        return $response->withHeader('Location', $this->router->pathFor('index'));
        // return $this->view->render($response, 'login.latte');
    })->setName('logout');

    // Delete user endpoint
    $app->get('/user/{id}/delete', function (Request $request, Response $response, $args) {
        try {
            $stmt = $this->db->prepare('DELETE FROM person WHERE id_person = :id');
            $stmt->bindParam(':id', $args['id']); #v args je to co je ve slozenych zavorkach linku
            $stmt->execute();
        } catch (Excepiton $e) {
            $this->logger->error($e);
        }
        return $response->withHeader('Location', $this->router->pathFor('users_list')); #presmeruje na stranku users
    })->setName('delete_user');

    // new user endpoint
    $app->get('/user/new', function (Request $request, Response $response, $args) {
        // odstraneni warningu ve formech
        $data['formData'] = [
            'first_name' => '',
            'last_name' => '',
            'nickname' => '',
            'gender' => '',
            'height' => '',
            'birth_day' => '',
            'street_name' => '',
            'street_number' => '',
            'city' => '',
            'zip' => ''
        ];
        return $this->view->render($response, 'user_new.latte');
    })->setName('user_new');

    // processing form endpoint
    $app->post('/user/new', function (Request $request, Response $response, $args) {
        $formData = $request->getParsedBody(); # ['first_name' => 'aaa', "last_name" => "bbb"]
        $id_location = null;

        if (empty($formData['first_name']) or empty($formData['last_name']) or empty($formData['nickname'])) {
            $data['message'] = 'Please fill request field!';
        } else {
            try {
                if (!empty($formData['street_number']) or !empty($formData['street_name']) or !empty($formData['city']) 
                    or !empty($formData['zip'])) {
                    // uzivatel vyplnil adresu, vkladame novy zaznam do tabulky location
                    $id_location = newLocation($this, $formData);
                }
                $stmt = $this->db->prepare('INSERT INTO person (first_name, last_name, nickname, gender, height, 
                    birth_day, id_location) VALUES (:fn, :ln, :nn, :gn, :ht, :bd, :idl)');
                $stmt->bindValue(':fn', $formData['first_name']);
                $stmt->bindValue(':ln', $formData['last_name']);
                $stmt->bindValue(':nn', $formData['nickname']);
                $stmt->bindValue(':gn', empty($formData['gender']) ? null : $formData['gender']);
                $stmt->bindValue(':ht', empty($formData['height']) ? null : $formData['height']);
                $stmt->bindValue(':bd', empty($formData['birth_daty']) ? null : $formData['birth_day']);
                $stmt->bindValue(':idl', $id_location);
                $stmt->execute();
                $data['message'] = 'User succesfully inserted';
            } catch (Exception $e) {
                $data['message'] = $e;
            }
        }
        $data['formData'] = $formData;
        return $this->view->render($response, 'user_new.latte', $data);
    });

})->add(function($request, $response, $next) {
    if (!empty($_SESSION['logged_user'])) {
        return $next($request, $response);
    } else {
        return $response->withHeader('Location', $this->router->pathFor('login_form'));
    }
});